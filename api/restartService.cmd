@echo off
title [Controller Toolkit] Restarting service
start api\enterCMode.cmd
ping 127.0.0.1 > nul
start api\stopService.cmd
ping 127.0.0.1 > nul
start api\startService.cmd
exit