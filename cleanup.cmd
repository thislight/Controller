@echo off
title [Controller] Cleaning up old files..
set cweb=%CD%\WEB
:initStep
if exist %cweb%\cmode.txt (del %CD%\WEB\cmode.txt)
if exist %cweb%\restartnow.txt goto :lastrestartnow
if exist %cweb%\stop_service.txt goto :lastStopService
goto :end
:gobackformlrsn
if exist %cweb%\shutdown.txt goto :lastshutdown
:gobackformlsd
if exist %cweb%\cmdrun.txt (del %cweb%\cmdrun.txt)
if exist %cweb%\CCM.txt (del %cweb%\CCM.txt)
if exist %cweb%\close.txt (del %cweb%\close.txt)
goto :initStep
:lastrestartnow
del %cweb%\restartnow.txt
if exist %cweb%\restartnow.txt set notdors=1
if not exist %cweb%\restartnow.txt set notdors=0
goto :gobackformlrsn
:lastshudown
del %cweb%\shutdown.txt
if exist %cweb%\shutdown.txt set notdors=1
if not exist %cweb%\shutdown.txt set notdors=0
goto :gobackformlsd
:lastStopService
del %cweb%\stop_service.txt
if exist %cweb%\stop_service.txt set notdors=1
if not exist %cweb%\stop_service.txt set notdors=0

:end
exit