@echo off
title [Controller] Console
:startup
:1
set/p runc=*
if exist %CD%\api\%runc%.cmd goto :runapicmd
if exist %CD%\api\%runc%.bat goto :runapibat
if exist %CD%\plugins\%runc%.cp goto :runplugins
goto :1
:runapicmd
start %CD%\api\%runc%.cmd
goto :1
:runapibat
start %CD%\api\%runc%.bat
goto :1
:runplugins
type %CD%\plugins\%runc%.cp>tmp.cmd
echo pause>tmp.cmd
start %CD%\tmp.cmd
goto :1