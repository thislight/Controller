@echo off
title [Controller] Server
:Startup
set cweb=%cd%\WEB
start makenewfile
start cleanup
echo Check network connectivity.
ping 127.0.0.1
echo [T:%TIME% D:%DATE%]Try start the server.>>api\log\log.log
:Checkthatthemode
echo [T:%TIME% D:%DATE%]Listening..>>api\log\log.log
if exist %cweb%\cmode.txt goto :cmode
ping 127.0.0.1>>nul
goto :Checkthatthemode
:cmode
echo [T:%TIME% D:%DATE%]Checking..>>api\log\log.log
:restart
if exist %cweb%\restartnow.txt api\restart
:shutdown
if exist %cweb%\shutdown.txt api\shutdown
:cmd
if exist %cweb%\cmdrun.txt api\cmdrun
:ifStopService
if exist %cweb%\stop_service.txt goto :closeService
:closecmode
if exist %cweb%\CCM.txt api\wlogccm
if exist %cweb%\CCM.txt goto :Checkthstthemode
ping 127.0.0.1>>nul
goto :cmode
:error

:closeService
echo [T:%TIME% D:%DATE%]Service will be stop>>api\log\log.log
goto :end

:end
copy api\log\log.log api\log\log2.log
start api\dellog.cmd
exit